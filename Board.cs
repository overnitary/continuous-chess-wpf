﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace c_chess
{
    class Board : Canvas
    {
        private Pen blackLinePen;
        private Pen dashLinePen;

        private Brush invalidPreviewColor = new SolidColorBrush(Color.FromRgb(0xff, 0x42, 0x42));
        private Brush backgroundBrush = new SolidColorBrush(Colors.White);
        private Brush blackPieceFill = new SolidColorBrush(Colors.DarkGray);
        private Brush whitePieceFill = new SolidColorBrush(Colors.White);
        private Brush selectedPieceFill = new SolidColorBrush(Colors.LightGray);
        private Pen normalPieceBorder = new Pen(Brushes.Black, 2);

        private Brush moveRangeBrush = new SolidColorBrush(Color.FromArgb(0x9a, 0xc8, 0xe0, 0xf4));
        private Brush takeRangeBrush = new SolidColorBrush(Color.FromArgb(0x9a, 0xff, 0xdf, 0xc5));
        private Brush dangerRangeBrush = new SolidColorBrush(Color.FromArgb(0x9a, 0xff, 0xbd, 0xbd));

        private Dictionary<Player, Dictionary<PieceType, BitmapImage>> pieceBitmaps;

        private double ratio;

        public Board()
        {
            InitializePieceBitmaps();

            ratio = (Math.Min(this.ActualWidth, this.ActualHeight) - 40) / 8.0;
            blackLinePen = new Pen(Brushes.Black, 2);
            dashLinePen = new Pen(Brushes.Black, 1);
            dashLinePen.DashStyle = new DashStyle(new double[] { 5, 5 }, 0);

            Game.Current().Control.View = this;

        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            ratio = (Math.Min(this.ActualWidth, this.ActualHeight) - 40) / 8.0;

            // If a piece is selected, draw its moving range
            DrawMoveRange(Game.Current().SelectedPiece, drawingContext);

            // Erase out-of-board paint
            drawingContext.DrawRectangle(backgroundBrush, null, new Rect(GameToCanvas(-2, -2), GameToCanvas(0, 10)));
            drawingContext.DrawRectangle(backgroundBrush, null, new Rect(GameToCanvas(-2, -2), GameToCanvas(10, 0)));
            drawingContext.DrawRectangle(backgroundBrush, null, new Rect(GameToCanvas(-2, 8), GameToCanvas(10, 10)));
            drawingContext.DrawRectangle(backgroundBrush, null, new Rect(GameToCanvas(8, -2), GameToCanvas(10, 10)));

            // Draw board grid
            for (int i=1; i<=8; i++)
            {
                drawingContext.DrawLine(dashLinePen, GameToCanvas(0.0, i), GameToCanvas(8.0, i));
                drawingContext.DrawLine(dashLinePen, GameToCanvas(i, 0.0), GameToCanvas(i, 8.0));
            }
            drawingContext.DrawLine(blackLinePen, GameToCanvas(-0.2, 0.0), GameToCanvas(8.2, 0.0));
            drawingContext.DrawLine(blackLinePen, GameToCanvas(0.0, -0.2), GameToCanvas(0.0, 8.2));

            // Draw all pieces (except the selected one, which we draw as cursor instead)
            foreach (var piece in Game.Current().Pieces)
            {
                if (piece == Game.Current().SelectedPiece)
                {
                    drawingContext.DrawLine(blackLinePen, GameToCanvas(piece.X - 0.1, piece.Y - 0.1), GameToCanvas(piece.X + 0.1, piece.Y + 0.1));
                    drawingContext.DrawLine(blackLinePen, GameToCanvas(piece.X + 0.1, piece.Y - 0.1), GameToCanvas(piece.X - 0.1, piece.Y + 0.1));
                    drawingContext.DrawEllipse(null, dashLinePen, GameToCanvas(piece.X, piece.Y), piece.Radius * ratio, piece.Radius * ratio);
                    continue;
                }
                DrawPiece(piece, drawingContext);
            }

            // If a piece is selected, draw movement preview at cursor
            if (Game.Current().SelectedPiece != null) DrawCursor(drawingContext);
            
            if (Game.Current().State == UIState.Promotion)
            {
                drawingContext.DrawRectangle(backgroundBrush, null, new Rect(
                    GameToCanvas(2.0 - Constants.DialogMarginInner - Constants.DialogMarginOuter, 3.5 - Constants.DialogMarginInner - Constants.DialogMarginOuter),
                    GameToCanvas(6.0 + Constants.DialogMarginInner + Constants.DialogMarginOuter, 4.5 + Constants.DialogMarginInner + Constants.DialogMarginOuter)));
                drawingContext.DrawRectangle(null, blackLinePen, new Rect(
                    GameToCanvas(2.0 - Constants.DialogMarginInner, 3.5 - Constants.DialogMarginInner),
                    GameToCanvas(6.0 + Constants.DialogMarginInner, 4.5 + Constants.DialogMarginInner)));
                foreach (var candidate in Game.Current().PromotionCandidates)
                {
                    DrawPiece(candidate, drawingContext);
                }
            }

            //foreach (var p in Game.Current().DebugPoints)
            //{
            //    drawingContext.DrawEllipse(null, blackLinePen, GameToCanvas(p.X, p.Y), 0.1, 0.1);
            //}
            base.OnRender(drawingContext);
        }

        public Point GameToCanvas(double x, double y)
        {
            return new Point(20 + x * ratio, 20 + (8.0 - y) * ratio);
        }

        public Point CanvasToGame(double x, double y)
        {
            return new Point((x - 20) / ratio, 8.0 - (y - 20) / ratio);
        }

        private void DrawPiece(Piece piece, DrawingContext drawingContext)
        {
            var brush = piece.Owner == Player.White ? whitePieceFill : blackPieceFill;
            var pen = blackLinePen;
            drawingContext.DrawEllipse(brush, pen, GameToCanvas(piece.X, piece.Y), piece.Radius * ratio, piece.Radius * ratio);
            drawingContext.DrawImage(pieceBitmaps[piece.Owner][piece.Type], new Rect(
                GameToCanvas(piece.X - piece.Radius, piece.Y - piece.Radius), 
                GameToCanvas(piece.X + piece.Radius, piece.Y + piece.Radius)));
        }

        private void DrawCursor(DrawingContext drawingContext)
        {
            var piece = Game.Current().SelectedPiece;
            var cursor = Game.Current().PreviewPiece;

            var previewLinePen = Game.Current().IsPreviewLegal() ? blackLinePen : new Pen(invalidPreviewColor, 2);
            if (piece.Type != PieceType.Knight)
                drawingContext.DrawLine(previewLinePen, GameToCanvas(piece.X, piece.Y), GameToCanvas(cursor.X, cursor.Y));

            var brush = piece.Owner == Player.White ? whitePieceFill : blackPieceFill;
            if (!Game.Current().IsPreviewLegal()) brush = invalidPreviewColor;
            drawingContext.DrawEllipse(brush, blackLinePen, GameToCanvas(cursor.X, cursor.Y), piece.Radius * ratio, piece.Radius * ratio);
            drawingContext.DrawImage(pieceBitmaps[piece.Owner][piece.Type], new Rect(
                GameToCanvas(cursor.X - piece.Radius, cursor.Y - piece.Radius), 
                GameToCanvas(cursor.X + piece.Radius, cursor.Y + piece.Radius)));
        }

        private void DrawMoveRange(Piece piece, DrawingContext drawingContext)
        {
            var conf = Game.Current().Config;
            if (piece == null) return;
            var pen = new Pen(moveRangeBrush, piece.Radius * 2 * ratio);
            switch (piece.Type)
            {
                case PieceType.Rook:
                    drawingContext.DrawLine(pen, GameToCanvas(piece.X - 8, piece.Y), GameToCanvas(piece.X + 8, piece.Y));
                    drawingContext.DrawLine(pen, GameToCanvas(piece.X, piece.Y - 8), GameToCanvas(piece.X, piece.Y + 8));
                    break;
                case PieceType.Bishop:
                    drawingContext.DrawLine(pen, GameToCanvas(piece.X - 8, piece.Y - 8), GameToCanvas(piece.X + 8, piece.Y + 8));
                    drawingContext.DrawLine(pen, GameToCanvas(piece.X - 8, piece.Y + 8), GameToCanvas(piece.X + 8, piece.Y - 8));
                    break;
                case PieceType.Queen:
                    drawingContext.DrawLine(pen, GameToCanvas(piece.X - 8, piece.Y), GameToCanvas(piece.X + 8, piece.Y));
                    drawingContext.DrawLine(pen, GameToCanvas(piece.X, piece.Y - 8), GameToCanvas(piece.X, piece.Y + 8));
                    drawingContext.DrawLine(pen, GameToCanvas(piece.X - 8, piece.Y - 8), GameToCanvas(piece.X + 8, piece.Y + 8));
                    drawingContext.DrawLine(pen, GameToCanvas(piece.X - 8, piece.Y + 8), GameToCanvas(piece.X + 8, piece.Y - 8));
                    break;
                case PieceType.Knight:
                    drawingContext.DrawEllipse(null, pen, GameToCanvas(piece.X, piece.Y), conf.KnightMoveRange * ratio, conf.KnightMoveRange * ratio);
                    break;
                case PieceType.King:
                    drawingContext.DrawEllipse(moveRangeBrush, null, GameToCanvas(piece.X, piece.Y), 
                        (conf.KingMoveRange + piece.Radius) * ratio, (conf.KingMoveRange + piece.Radius) * ratio);
                    break;
                case PieceType.Pawn:
                    drawingContext.DrawEllipse(moveRangeBrush, null, 
                        GameToCanvas(piece.X, piece.Y + (piece.Owner == Player.White? conf.PawnMoveCentralDistance : -conf.PawnMoveCentralDistance)),
                        (conf.PawnMoveRadius + piece.Radius) * ratio, (conf.PawnMoveRadius + piece.Radius) * ratio);
                    double t = conf.PawnMoveCentralDistance / Math.Sqrt(2);
                    drawingContext.DrawEllipse(takeRangeBrush, null,
                        GameToCanvas(piece.X + t, piece.Y + (piece.Owner == Player.White ? t: -t)),
                        (conf.PawnMoveRadius + piece.Radius) * ratio, (conf.PawnMoveRadius + piece.Radius) * ratio);
                    drawingContext.DrawEllipse(takeRangeBrush, null,
                        GameToCanvas(piece.X - t, piece.Y + (piece.Owner == Player.White ? t : -t)),
                        (conf.PawnMoveRadius + piece.Radius) * ratio, (conf.PawnMoveRadius + piece.Radius) * ratio);
                    break;
            }
        }

        private void InitializePieceBitmaps()
        {
            pieceBitmaps = new Dictionary<Player, Dictionary<PieceType, BitmapImage>>();
            pieceBitmaps[Player.White] = new Dictionary<PieceType, BitmapImage>();
            pieceBitmaps[Player.Black] = new Dictionary<PieceType, BitmapImage>();
            LoadPieceBitmap(Player.White, PieceType.Pawn, @"Assets/WP.png");
            LoadPieceBitmap(Player.White, PieceType.Knight, @"Assets/WN.png");
            LoadPieceBitmap(Player.White, PieceType.Bishop, @"Assets/WB.png");
            LoadPieceBitmap(Player.White, PieceType.Rook, @"Assets/WR.png");
            LoadPieceBitmap(Player.White, PieceType.Queen, @"Assets/WQ.png");
            LoadPieceBitmap(Player.White, PieceType.King, @"Assets/WK.png");
            LoadPieceBitmap(Player.Black, PieceType.Pawn, @"Assets/BP.png");
            LoadPieceBitmap(Player.Black, PieceType.Knight, @"Assets/BN.png");
            LoadPieceBitmap(Player.Black, PieceType.Bishop, @"Assets/BB.png");
            LoadPieceBitmap(Player.Black, PieceType.Rook, @"Assets/BR.png");
            LoadPieceBitmap(Player.Black, PieceType.Queen, @"Assets/BQ.png");
            LoadPieceBitmap(Player.Black, PieceType.King, @"Assets/BK.png");
        }

        private void LoadPieceBitmap(Player player, PieceType piece, string uri)
        {
            var bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(uri, UriKind.RelativeOrAbsolute);
            bi.EndInit();
            pieceBitmaps[player][piece] = bi;
        }
    }
}
