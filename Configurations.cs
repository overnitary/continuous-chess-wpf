﻿using System;
using System.Collections.Generic;
using System.Text;

namespace c_chess
{
    public class Configurations
    {
        public double PieceRadius = Math.Sqrt(2) / 4 * 0.9;
        public double KnightMoveRange = Math.Sqrt(5);
        public double KingMoveRange = 1.0;
        public double PawnMoveCentralDistance = 1.25;
        public double PawnMoveRadius = 0.25;

        public List<Piece> InitialBoardState;

        public static Configurations Default()
        {
            Configurations conf = new Configurations();
            conf.PieceRadius = Math.Sqrt(2) / 4 * 0.9;
            conf.KnightMoveRange = Math.Sqrt(5);
            conf.KingMoveRange = 1.0;
            conf.PawnMoveCentralDistance = 1.25;
            conf.PawnMoveRadius = 0.25;

            var pieces = new List<Piece>();
            pieces.Add(new Piece { Owner = Player.White, Type = PieceType.Rook, X = 0.5, Y = 0.5 });
            pieces.Add(new Piece { Owner = Player.White, Type = PieceType.Rook, X = 7.5, Y = 0.5 });
            pieces.Add(new Piece { Owner = Player.White, Type = PieceType.Knight, X = 1.5, Y = 0.5 });
            pieces.Add(new Piece { Owner = Player.White, Type = PieceType.Knight, X = 6.5, Y = 0.5 });
            pieces.Add(new Piece { Owner = Player.White, Type = PieceType.Bishop, X = 2.5, Y = 0.5 });
            pieces.Add(new Piece { Owner = Player.White, Type = PieceType.Bishop, X = 5.5, Y = 0.5 });
            pieces.Add(new Piece { Owner = Player.White, Type = PieceType.Queen, X = 3.5, Y = 0.5 });
            pieces.Add(new Piece { Owner = Player.White, Type = PieceType.King, X = 4.5, Y = 0.5 });
            pieces.Add(new Piece { Owner = Player.Black, Type = PieceType.Rook, X = 0.5, Y = 7.5 });
            pieces.Add(new Piece { Owner = Player.Black, Type = PieceType.Rook, X = 7.5, Y = 7.5 });
            pieces.Add(new Piece { Owner = Player.Black, Type = PieceType.Knight, X = 1.5, Y = 7.5 });
            pieces.Add(new Piece { Owner = Player.Black, Type = PieceType.Knight, X = 6.5, Y = 7.5 });
            pieces.Add(new Piece { Owner = Player.Black, Type = PieceType.Bishop, X = 2.5, Y = 7.5 });
            pieces.Add(new Piece { Owner = Player.Black, Type = PieceType.Bishop, X = 5.5, Y = 7.5 });
            pieces.Add(new Piece { Owner = Player.Black, Type = PieceType.Queen, X = 3.5, Y = 7.5 });
            pieces.Add(new Piece { Owner = Player.Black, Type = PieceType.King, X = 4.5, Y = 7.5 });
            for (double i = 0.5; i < 8; i += 1)
            {
                pieces.Add(new Piece { Owner = Player.White, Type = PieceType.Pawn, X = i, Y = 1.5 });
                pieces.Add(new Piece { Owner = Player.Black, Type = PieceType.Pawn, X = i, Y = 6.5 });
            }

            conf.InitialBoardState = pieces;
            return conf;
        }
    }
}
