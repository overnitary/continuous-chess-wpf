﻿using System;
using System.Collections.Generic;
using System.Text;

namespace c_chess
{
    class Constants
    {
        public static readonly double Sigma = 0.001;

        public static readonly double DialogMarginInner = 0.2;
        public static readonly double DialogMarginOuter = 0.2;

        public static readonly string ConfigFilePath = "conf.xml";
    }
}
