﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Xml.Serialization;

namespace c_chess
{
    public enum Player
    {
        White,
        Black
    }

    public enum UIState
    {
        Idle,
        PieceSelected,
        Promotion
    }

    // Singleton class maintaining all game states.
    class Game
    {
        private static Game instance = new Game();

        public static Game Current()
        {
            return instance;
        }

        private Game()
        {
            State = UIState.Idle;
            SelectedPiece = null;
            ActivePlayer = Player.White;
            Control = new GameControl();
            LoadConfigurations();
            Pieces = Config.InitialBoardState;

            DebugPoints = new List<Point>();
        }

        public GameControl Control { get; private set; }
        public Player ActivePlayer { get; private set; }
        public UIState State { get; private set; }
        public List<Piece> Pieces { get; private set; }

        public Configurations Config { get; private set; }

        public Piece SelectedPiece { get; private set; }
        // A virtual piece that previews current selected piece at cursor location (after enforced to the closest point in its moving range).
        // Only applicable when SelectedPiece != null.
        public Piece PreviewPiece { get; private set; }
        public List<Piece> PromotionCandidates { get; private set; }

        public List<Point> DebugPoints { get; private set; }

        public void SelectPiece(Piece piece, Point cursor)
        {
            Debug.Assert(State == UIState.Idle);
            SelectedPiece = piece;
            PreviewPiece = piece.CloneAtPoint(piece.EnforceMoveRange(cursor));
            State = UIState.PieceSelected;
        }

        public void UnselectPiece()
        {
            Debug.Assert(State == UIState.PieceSelected);
            State = UIState.Idle;
            SelectedPiece = null;
            PreviewPiece = null;
        }

        public void UpdateCursor(Point cursor)
        {
            if (State != UIState.PieceSelected) return;
            Point pEnforcedCursor = SelectedPiece.EnforceMoveRange(cursor);
            PreviewPiece.X = pEnforcedCursor.X;
            PreviewPiece.Y = pEnforcedCursor.Y;
        }

        public void AttempMove()
        {
            if (!IsPreviewLegal()) return;

            // Take covered pieces
            Pieces.RemoveAll(piece => PreviewPiece.IsTaking(piece));

            if (PreviewPiece.IsPromoting())
            {
                PreparePromotion();
            }
            else
            {
                // Move to cursor position
                SelectedPiece.X = PreviewPiece.X;
                SelectedPiece.Y = PreviewPiece.Y;
                SelectedPiece = null;
                PreviewPiece = null;
                State = UIState.Idle;
            }
        }

        public bool IsPreviewLegal()
        {
            if (SelectedPiece == null) return true;
            if (State == UIState.Promotion) return true;
            return SelectedPiece.CanMoveTo(new Point(PreviewPiece.X, PreviewPiece.Y), Pieces);
        }

        public void PreparePromotion()
        {
            Debug.Assert(State == UIState.PieceSelected);
            PromotionCandidates = new List<Piece>();
            PromotionCandidates.Add(new Piece { Owner = SelectedPiece.Owner, Type = PieceType.Queen, X = 2.5, Y = 4 });
            PromotionCandidates.Add(new Piece { Owner = SelectedPiece.Owner, Type = PieceType.Rook, X = 3.5, Y = 4 });
            PromotionCandidates.Add(new Piece { Owner = SelectedPiece.Owner, Type = PieceType.Knight, X = 4.5, Y = 4 });
            PromotionCandidates.Add(new Piece { Owner = SelectedPiece.Owner, Type = PieceType.Bishop, X = 5.5, Y = 4 });
            State = UIState.Promotion;
        }

        public void ConfirmPromotion(Piece target)
        {
            SelectedPiece.Type = target.Type;
            SelectedPiece.X = PreviewPiece.X;
            SelectedPiece.Y = PreviewPiece.Y;
            SelectedPiece = null;
            PreviewPiece = null;
            State = UIState.Idle;
        }

        public void CancelPromotion()
        {
            Debug.Assert(State == UIState.Promotion);
            State = UIState.PieceSelected;
        }

        private void LoadConfigurations()
        {
            Config = Configurations.Default();
            var xmlSerializer = new XmlSerializer(typeof(Configurations));
            if (File.Exists(Constants.ConfigFilePath))
            {
                
                var xmlSettings = new System.Xml.XmlReaderSettings()
                {
                    CheckCharacters = false,
                };
                using (var streamReader = new StreamReader(Constants.ConfigFilePath, Encoding.UTF8))
                using (var xmlReader
                        = System.Xml.XmlReader.Create(streamReader, xmlSettings))
                {
                    Config = (Configurations)xmlSerializer.Deserialize(xmlReader); // （3）
                }
            }
            else
            {
                // Write a default configure file
                var xmlS = new XmlSerializer(typeof(Configurations));
                using (var streamWriter = new StreamWriter(Constants.ConfigFilePath, false, Encoding.UTF8))
                {
                    xmlS.Serialize(streamWriter, Config);
                    streamWriter.Flush();
                }
            }
        }
    }
}
