﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace c_chess
{
    class GameControl
    {
        public Board View { get; set; }

        public void MouseLeftButtonDown(Point pCursor)
        {
            var game = Game.Current();
            game.DebugPoints.Add(pCursor);
            switch (game.State)
            {
                case UIState.Idle:
                    var selectedPiece = game.Pieces.FindLast(piece => piece.Cover(pCursor));
                    if (selectedPiece != null) game.SelectPiece(selectedPiece, pCursor);
                    break;
                case UIState.PieceSelected:
                    game.UpdateCursor(pCursor);
                    game.AttempMove();
                    break;
                case UIState.Promotion:
                    var selectedCandidate = game.PromotionCandidates.FindLast(piece => piece.Cover(pCursor));
                    if (selectedCandidate != null) game.ConfirmPromotion(selectedCandidate);
                    break;
            }
            View.InvalidateVisual();
        }

        public void MouseMove(Point pCursor)
        {
            if (Game.Current().State == UIState.PieceSelected)
            {
                Game.Current().UpdateCursor(pCursor);
                View.InvalidateVisual();
            }
        }

        public void MouseRightButtonDown()
        {
            switch (Game.Current().State)
            {
                case UIState.PieceSelected:
                    Game.Current().UnselectPiece();
                    View.InvalidateVisual();
                    break;
                case UIState.Promotion:
                    // TODO: CancelPromotion is not working correctly now. Need to backup taken pieces and restore them upon cancel.
                    //Game.Current().CancelPromotion();
                    //View.InvalidateVisual();
                    break;
            }
        }
    }
}
