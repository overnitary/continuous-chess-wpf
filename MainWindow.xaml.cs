﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace c_chess
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void canvasBoard_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point pCanvas = e.GetPosition(canvasBoard);
            Point pGame = canvasBoard.CanvasToGame(pCanvas.X, pCanvas.Y);
            Game.Current().Control.MouseLeftButtonDown(pGame);
        }

        private void canvasBoard_MouseMove(object sender, MouseEventArgs e)
        {
            Point pCanvas = e.GetPosition(canvasBoard);
            Point pGame = canvasBoard.CanvasToGame(pCanvas.X, pCanvas.Y);
            Game.Current().Control.MouseMove(pGame);
        }

        private void canvasBoard_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Game.Current().Control.MouseRightButtonDown();
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            canvasBoard.Width = Math.Min(e.NewSize.Width, e.NewSize.Height);
            canvasBoard.Height = Math.Min(e.NewSize.Width, e.NewSize.Height);
        }
    }
}
