﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace c_chess
{

    public enum PieceType
    {
        Pawn,
        Knight,
        Bishop,
        Rook,
        Queen,
        King
    }

    public class Piece
    {
        public double X;
        public double Y;
        public Player Owner;
        public PieceType Type;
        public PieceType OriginalType;      // Before promotion
        public double Radius { get { return Game.Current().Config.PieceRadius; } }

        public Piece()
        {

        }

        public Piece CloneAtPoint(Point p)
        {
            return new Piece
            {
                Owner = this.Owner,
                Type = this.Type,
                OriginalType = this.OriginalType,
                X = p.X,
                Y = p.Y
            };
        }

        public bool Cover(Point p)
        {
            return (p.X - X) * (p.X - X) + (p.Y - Y) * (p.Y - Y) <= Radius * Radius;
        }

        public bool Overlap(Piece p)
        {
            return (p.X - X) * (p.X - X) + (p.Y - Y) * (p.Y - Y) <= (Radius + p.Radius) * (Radius + p.Radius);
        }

        public Point EnforceMoveRange(Point p)
        {
            var conf = Game.Current().Config;
            Point result = p;
            double dX = p.X - X;
            double dY = p.Y - Y;
            double d = Math.Sqrt(dX * dX + dY * dY);
            double theta = Math.Atan2(dY, dX);

            double newTheta;
            double newD;
            switch (Type)
            {
                case PieceType.Rook:
                    newTheta = Math.Round(theta / (Math.PI / 2)) * (Math.PI / 2);
                    newD = FitDOnBoard(X, Y, d, newTheta);
                    result.X = X + newD * Math.Cos(newTheta);
                    result.Y = Y + newD * Math.Sin(newTheta);
                    break;
                case PieceType.Bishop:
                    newTheta = (Math.Round(theta / (Math.PI / 2) - 0.5) + 0.5) * (Math.PI / 2);
                    newD = FitDOnBoard(X, Y, d, newTheta);
                    result.X = X + newD * Math.Cos(newTheta);
                    result.Y = Y + newD * Math.Sin(newTheta);
                    break;
                case PieceType.Queen:
                    newTheta = Math.Round(theta / (Math.PI / 4)) * (Math.PI / 4);
                    newD = FitDOnBoard(X, Y, d, newTheta);
                    result.X = X + newD * Math.Cos(newTheta);
                    result.Y = Y + newD * Math.Sin(newTheta);
                    break;
                case PieceType.Knight:
                    newTheta = FitThetaOnBoard(X, Y, theta);
                    result.X = X + conf.KnightMoveRange * Math.Cos(newTheta);
                    result.Y = Y + conf.KnightMoveRange * Math.Sin(newTheta);
                    break;
                case PieceType.King:
                    newD = Math.Min(d, conf.KingMoveRange);
                    result.X = X + newD * Math.Cos(theta);
                    result.Y = Y + newD * Math.Sin(theta);
                    result.X = Math.Max(result.X, Radius);
                    result.X = Math.Min(result.X, 8 - Radius);
                    result.Y = Math.Max(result.Y, Radius);
                    result.Y = Math.Min(result.Y, 8 - Radius);
                    break;
                case PieceType.Pawn:
                    double t = conf.PawnMoveCentralDistance / Math.Sqrt(2);
                    double[] centerXs = { X, X - t, X + t };
                    double[] centerYs = { Y + (Owner == Player.White ? conf.PawnMoveCentralDistance : -conf.PawnMoveCentralDistance),
                        Y + (Owner == Player.White ? t: -t), Y + (Owner == Player.White ? t: -t)};
                    double[] ds = new double[3];
                    double minD = 8.0;
                    int minI = 0;
                    for (int i=0; i<3; i++)
                    {
                        ds[i] = Math.Sqrt((p.X - centerXs[i]) * (p.X - centerXs[i]) + (p.Y - centerYs[i])* (p.Y - centerYs[i]));
                        if (ds[i] < minD)
                        {
                            minD = ds[i];
                            minI = i;
                        }
                    }
                    if (minD > conf.PawnMoveRadius)
                    {
                        theta = Math.Atan2(p.Y - centerYs[minI], p.X - centerXs[minI]);
                        result.X = centerXs[minI] + conf.PawnMoveRadius * Math.Cos(theta);
                        result.Y = centerYs[minI] + conf.PawnMoveRadius * Math.Sin(theta);
                    }
                    result.X = Math.Max(result.X, Radius);
                    result.X = Math.Min(result.X, 8 - Radius);
                    result.Y = Math.Max(result.Y, Radius);
                    result.Y = Math.Min(result.Y, 8 - Radius);
                    break;
            }
            return result;
        }

        private double FitDOnBoard(double X, double Y, double d, double theta)
        {
            if (X + d * Math.Cos(theta) > 8 - Radius) d = (8 - Radius - X) / Math.Cos(theta);
            if (Y + d * Math.Sin(theta) > 8 - Radius) d = (8 - Radius - Y) / Math.Sin(theta);
            if (X + d * Math.Cos(theta) < Radius) d = (X - Radius) / -Math.Cos(theta);
            if (Y + d * Math.Sin(theta) < Radius) d = (Y - Radius) / -Math.Sin(theta);
            return d;
        }

        private double FitThetaOnBoard(double X, double Y, double theta)
        {
            var conf = Game.Current().Config;
            // Search counterclockwise
            double newTheta1 = theta;
            if (X + conf.KnightMoveRange * Math.Cos(newTheta1) > 8 - Radius)
                newTheta1 = Math.Acos((8 - Radius - X) / conf.KnightMoveRange);
            if (Y + conf.KnightMoveRange * Math.Sin(newTheta1) > 8 - Radius)
                newTheta1 = Math.PI - Math.Asin((8 - Radius - Y) / conf.KnightMoveRange);
            if (X + conf.KnightMoveRange * Math.Cos(newTheta1) < Radius)
                newTheta1 = -Math.PI + Math.Acos((X - Radius) / conf.KnightMoveRange);
            if (Y + conf.KnightMoveRange * Math.Sin(newTheta1) < Radius)
                newTheta1 = -Math.Asin((Y - Radius) / conf.KnightMoveRange);
            if (X + conf.KnightMoveRange * Math.Cos(newTheta1) > 8 - Radius)
                newTheta1 = Math.Acos((8 - Radius - X) / conf.KnightMoveRange);
            // Search clockwise
            double newTheta2 = theta;
            if (X + conf.KnightMoveRange * Math.Cos(newTheta2) > 8 - Radius)
                newTheta2 = -Math.Acos((8 - Radius - X) / conf.KnightMoveRange);
            if (Y + conf.KnightMoveRange * Math.Sin(newTheta2) < Radius)
                newTheta2 = -Math.PI + Math.Asin((Y - Radius) / conf.KnightMoveRange);
            if (X + conf.KnightMoveRange * Math.Cos(newTheta2) < Radius)
                newTheta2 = Math.PI - Math.Acos((X - Radius) / conf.KnightMoveRange);
            if (Y + conf.KnightMoveRange * Math.Sin(newTheta2) > 8 - Radius)
                newTheta2 = Math.Asin((8 - Radius - Y) / conf.KnightMoveRange);
            if (X + conf.KnightMoveRange * Math.Cos(newTheta2) > 8 - Radius)
                newTheta2 = -Math.Acos((8 - Radius - X) / conf.KnightMoveRange);
            // Choose the one with less difference
            double delta1 = Math.Abs(theta - newTheta1);
            if (delta1 > Math.PI) delta1 = 2 * Math.PI - delta1;
            double delta2 = Math.Abs(theta - newTheta2);
            if (delta2 > Math.PI) delta2 = 2 * Math.PI - delta2;
            return delta1 < delta2 ? newTheta1 : newTheta2;
        }

        public bool IsTaking(Piece target)
        {
            return target.Owner != this.Owner && this.Cover(new Point(target.X, target.Y));
        }

        // Check if current piece can move to target point. Note that target point is after movement enforcement,
        // so this function only calulates interference from other pieces.
        public bool CanMoveTo(Point p, List<Piece> pieces)
        {
            var conf = Game.Current().Config;
            Piece tempPiece = this.CloneAtPoint(p);
            List<Piece> overlappedPieces = pieces.FindAll(piece => piece != this && piece.Overlap(tempPiece));
            if (overlappedPieces.Count > 1) return false;

            // Pieces that matter are pieces not current one and cannot be captured on target position
            List<Piece> matteredPieces = pieces.FindAll(piece =>
                piece != this && !tempPiece.IsTaking(piece));

            if (Type == PieceType.Pawn)
            {
                double t = conf.PawnMoveCentralDistance / Math.Sqrt(2);
                double[] centerXs = { X, X - t, X + t };
                double[] centerYs = { Y + (Owner == Player.White ? conf.PawnMoveCentralDistance : -conf.PawnMoveCentralDistance),
                        Y + (Owner == Player.White ? t: -t), Y + (Owner == Player.White ? t: -t)};
                double[] ds = new double[3];
                for (int i = 0; i < 3; i++)
                {
                    ds[i] = Math.Sqrt((p.X - centerXs[i]) * (p.X - centerXs[i]) + (p.Y - centerYs[i]) * (p.Y - centerYs[i]));
                }
                bool allowMove = ds[0] < conf.PawnMoveRadius + Constants.Sigma;
                bool allowTake = ds[1] < conf.PawnMoveRadius + Constants.Sigma || ds[2] <= conf.PawnMoveRadius + Constants.Sigma;
                if (!allowMove && !allowTake)
                {
                    return false;
                }
                if (!allowTake)
                {
                    // If in move-only zone all eneny pieces qualify for blocking
                    matteredPieces = pieces.FindAll(piece => piece != this);
                }
                if (!allowMove)
                {
                    // If in take-only zone and no takable pieces then return false
                    if (pieces.FindLast(piece => tempPiece.IsTaking(piece)) == null) return false;
                }
            }
            foreach (var piece in matteredPieces)
            {
                if (tempPiece.Overlap(piece)) return false;
            }
            if (Type == PieceType.Knight) return true;
            double dX = p.X - X;
            double dY = p.Y - Y;
            foreach (var piece in matteredPieces)
            {
                double u = ((piece.X - X) * dX + (piece.Y - Y) * dY) / (dX * dX + dY * dY);
                if (u < 0 || u > 1) continue;
                double footX = X + u * dX;
                double footY = Y + u * dY;
                if ((piece.X - footX) * (piece.X - footX) + (piece.Y - footY) * (piece.Y - footY) < 4 * Radius * Radius) return false;
            }
            return true;
        }

        public bool IsPromoting()
        {
            if (Type != PieceType.Pawn) return false;
            return Owner == Player.White && Y - Radius >= 7.0 || Owner == Player.Black && Y + Radius <= 1.0;
        }
    }
}
